package com.mastertech.microservice_pergunta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class App 
{
	
    public static void main( String[] args )
    {
       // Iniciando microsservico Perguntas 
       SpringApplication.run(App.class, args);
    }
}

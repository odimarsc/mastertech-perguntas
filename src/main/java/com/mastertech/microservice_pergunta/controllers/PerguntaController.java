package com.mastertech.microservice_pergunta.controllers;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mastertech.microservice_pergunta.models.Pergunta;
import com.mastertech.microservice_pergunta.repositories.PerguntaRepository;

@RestController
public class PerguntaController {
	
	@Autowired
	PerguntaRepository perguntaRepository;
	
	@RequestMapping(method = RequestMethod.POST, path = "/pergunta")
	public Pergunta criarPergunta(@RequestBody Pergunta pergunta) {
		return perguntaRepository.save(pergunta);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/perguntas")
	public Pergunta[] criarPerguntas(@RequestBody Pergunta pergunta[]) {
		for (int j = 0; j < pergunta.length; j++) {
			perguntaRepository.save(pergunta[j]);
		}
		return pergunta;
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/pergunta/{qtde}")
	public ArrayList<Pergunta> buscarPerguntas(@PathVariable Integer qtde) {
		ArrayList<Pergunta> listaTodasPerguntas = new ArrayList<>();
		ArrayList<Pergunta> listaPerguntasSelecionas = new ArrayList<>();
		listaTodasPerguntas = (ArrayList<Pergunta>) perguntaRepository.findAll();
//		Collections.shuffle(listaTodasPerguntas);
		
		for (int i = 0; i < qtde; i++) {
			int numero = (int) Math.floor(Math.random() * listaTodasPerguntas.size());
			Pergunta pergunta = listaTodasPerguntas.remove(numero);
			listaPerguntasSelecionas.add(pergunta);
		}
		return listaPerguntasSelecionas;

		}		
}

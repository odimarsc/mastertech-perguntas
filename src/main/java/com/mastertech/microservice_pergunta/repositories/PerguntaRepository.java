package com.mastertech.microservice_pergunta.repositories;

import org.springframework.data.repository.CrudRepository;
import com.mastertech.microservice_pergunta.models.Pergunta;

public interface PerguntaRepository extends CrudRepository<Pergunta, Long> {
	
}

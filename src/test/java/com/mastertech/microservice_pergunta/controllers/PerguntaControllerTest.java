package com.mastertech.microservice_pergunta.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastertech.microservice_pergunta.models.Pergunta;
import com.mastertech.microservice_pergunta.repositories.PerguntaRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(PerguntaController.class)
public class PerguntaControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	PerguntaRepository perguntaRepository;

	ObjectMapper mapper = new ObjectMapper();

	@Test
	public void testaCriaPergunta() throws Exception {

		Pergunta pergunta = new Pergunta();
		
		pergunta.setId(100);
		pergunta.setTitulo("Qual ano?");
		pergunta.setResposta(4);
		pergunta.setOpcao1("2010");
		pergunta.setOpcao2("2011");
		pergunta.setOpcao3("2014");
		pergunta.setOpcao4("2018");
		pergunta.setCategoria("Teste");

		when(perguntaRepository.save(any(Pergunta.class))).thenReturn(pergunta);
		String json = mapper.writeValueAsString(pergunta);

		mockMvc.perform(post("/pergunta")
			.content(json).contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.titulo", equalTo("Qual ano?")))
			.andExpect(jsonPath("$.resposta", equalTo(4)));
}

	@Test
	public void testaCriaListaPerguntas() throws Exception {

		Pergunta pergunta1 = new Pergunta();
		pergunta1.setTitulo("Qual ano?");
		pergunta1.setResposta(4);
		pergunta1.setOpcao1("2010");
		pergunta1.setOpcao2("2011");
		pergunta1.setOpcao3("2014");
		pergunta1.setOpcao4("2018");
		pergunta1.setCategoria("Teste");

		Pergunta pergunta2 = new Pergunta();
		pergunta2.setTitulo("Quanto signos existem?");
		pergunta2.setResposta(1);
		pergunta2.setOpcao1("12");
		pergunta2.setOpcao2("10");
		pergunta2.setOpcao3("13");
		pergunta2.setOpcao4("9");
		pergunta2.setCategoria("Teste");
		
		List<Pergunta> listaPerguntas = new ArrayList<>();
		listaPerguntas.add(pergunta1);
		listaPerguntas.add(pergunta2);

		when(perguntaRepository.save(any(Pergunta.class))).thenReturn(listaPerguntas.get(0), listaPerguntas.get(1));
		String json = mapper.writeValueAsString(listaPerguntas);

		mockMvc.perform(post("/perguntas")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].titulo", equalTo("Qual ano?")))
				.andExpect(jsonPath("$[0].resposta", equalTo(4)))
				.andExpect(jsonPath("$[0].opcao1", equalTo("2010")))
				.andExpect(jsonPath("$[0].opcao2", equalTo("2011")))
				.andExpect(jsonPath("$[0].opcao3", equalTo("2014")))
				.andExpect(jsonPath("$[0].opcao4", equalTo("2018")))
				.andExpect(jsonPath("$[0].categoria", equalTo("Teste")))
				.andExpect(jsonPath("$[1].titulo", equalTo("Quanto signos existem?")))
				.andExpect(jsonPath("$[1].resposta", equalTo(1)))
				.andExpect(jsonPath("$[1].opcao1", equalTo("12")))
				.andExpect(jsonPath("$[1].opcao2", equalTo("10")))
				.andExpect(jsonPath("$[1].opcao3", equalTo("13")))
				.andExpect(jsonPath("$[1].opcao4", equalTo("9")))
				.andExpect(jsonPath("$[1].categoria", equalTo("Teste")));
}
	
	@Test
	public void testaBuscaPerguntas() throws Exception {
		
			Pergunta pergunta1 = new Pergunta();
			
			pergunta1.setTitulo("Oi");
			pergunta1.setResposta(4);
			pergunta1.setOpcao1("2010");
			pergunta1.setOpcao2("2011");
			pergunta1.setOpcao3("2014");
			pergunta1.setOpcao4("2018");
			pergunta1.setCategoria("Teste");
	
			Pergunta pergunta2 = new Pergunta();
			pergunta2.setTitulo("Tudo bem?");
			pergunta2.setResposta(1);
			pergunta2.setOpcao1("12");
			pergunta2.setOpcao2("10");
			pergunta2.setOpcao3("13");
			pergunta2.setOpcao4("9");
			pergunta2.setCategoria("Teste");
			
			ArrayList<Pergunta> perguntas = new ArrayList<>();
			perguntas.add(pergunta1);
			perguntas.add(pergunta2);
				
			when(perguntaRepository.findAll()).thenReturn(perguntas);
					
		}
}

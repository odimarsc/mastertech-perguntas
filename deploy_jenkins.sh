#!/bin/bash

#IP do servidor da Digital Ocean
IP_REMOTO=142.93.83.244

if [ $1 ]; then
   IP_REMOTO = $1
   echo "ip remoto informado: $IP_REMOTO"
fi

scp jenkins.pp root@$IP_REMOTO:
ssh root@$IP_REMOTO 'dnf install -y puppet'
ssh root@$IP_REMOTO 'puppet apply jenkins.pp'
echo 'Here is the initial admin password:'
ssh root@$IP_REMOTO 'cat /var/lib/jenkins/secrets/initialAdminPassword'

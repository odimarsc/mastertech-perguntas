package {'java-1.8.0-openjdk':
  ensure => latest,
}
package {'mockito':
  ensure => latest,
}
package {'git':
  ensure => latest,
}
package {'maven':
  ensure => latest,
}
file {'install jenkins repository':
  path => '/etc/yum.repos.d/jenkins.repo',
  ensure => file,
  source => 'http://pkg.jenkins-ci.org/redhat/jenkins.repo',
}
exec {'install jenkins':
  command => '/usr/bin/rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key',
}
package {'jenkins':
  ensure => latest,
}
service {'jenkins':
  ensure => running,
  enable => true,
}



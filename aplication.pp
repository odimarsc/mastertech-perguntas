package {'java-1.8.0-openjdk':
  ensure => latest,
}
package {'vim':
  ensure => latest,
}
package {'mariadb':
  ensure => latest,
}
package {'mariadb-server':
  ensure => latest,
}
service {'mariadb':
  ensure => running,
  enable => true,
}
exec {'create database jogo':
command =>  '/usr/bin/mysql -u root --execute="create database if not exists jogo"',
} 
exec {'show databases':
command =>  '/usr/bin/mysql -u root --execute="show databases"',
}


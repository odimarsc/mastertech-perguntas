#!/bin/bash

#IP do servidor da Digital Ocean
IP_REMOTO=142.93.87.42

if [ $1 ]; then
   IP_REMOTO = $1
   echo "ip remoto informado: $IP_REMOTO"
fi

scp aplication.pp root@$IP_REMOTO:
ssh root@$IP_REMOTO 'dnf install -y puppet'
ssh root@$IP_REMOTO 'puppet apply aplication.pp'
